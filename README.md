# uLVRB_Isense project :

- Schematics based on ADP7158 LDO regulators to generate 1.2V and 2.5V from 3.3V AC/DC power plug.<br>
- Current sensing by LTC2945. Possibility to read the current value and put some alert level by I2C protocol.<br>
- I2C bus (2.5V) to be plugged on the GPIO connector of the FEAD or VICE++ board.
- Corresponding firmware to be found on the FEAD project page : https://gitlab.cern.ch/dejardin/fead<br>

M.D.

Initial creation : 2019/06/05 <br>
Last Modification : 2019/06/05

